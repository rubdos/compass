use anyhow::Error;
use sonarr::*;

fn default_client() -> Client<String, String> {
    let server = dotenv::var("SONARR_URL").unwrap();
    let api_key = dotenv::var("SONARR_API").unwrap();

    Client::new(server, api_key)
}

#[tokio::test]
async fn fetch_series() -> Result<(), Error> {
    let client = default_client();
    let all_series = client.all_series().await?;

    // Fetch every series on itself.
    for series in all_series.iter() {
        let id = match series.id {
            Some(id) => id,
            None => continue,
        };
        let _show = client.series(id).await?;
    }
    Ok(())
}

#[tokio::test]
async fn simple_get() -> Result<(), Error> {
    let client = default_client();

    macro_rules! do_test {
        ($($method:ident,)*) => {
            $(do_test!(IMPL $method);)*
        };
        (IMPL $method:ident) => {
            println!("Testing {}", stringify!($method));
            let _result = client.$method().await?;
        };
    }

    do_test! {
        system_status,
        all_series,
        calendar,
    }
    Ok(())
}

#[tokio::test]
async fn calendar_range() -> Result<(), Error> {
    // use chrono::prelude::*;

    let client = default_client();

    // XXX: hard coded days
    // TODO: range today to today + 1 month
    let _calendar = client
        .calendar_between("2019-03-30".into(), "2019-04-30".into())
        .await?;
    Ok(())
}
