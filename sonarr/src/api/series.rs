use chrono::prelude::*;
use serde::{Deserialize, Deserializer};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SeriesDetails<S> {
    pub alternate_titles: Vec<AlternateTitle<S>>,
    pub total_episode_count: usize,
    pub episode_count: usize,
    pub episode_file_count: usize,
    /// Size in bytes on disk
    pub size_on_disk: u64,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Series<S> {
    pub title: S,
    pub sort_title: S,
    pub season_count: usize,
    pub status: SeriesStatus,
    pub overview: Option<S>,
    pub next_airing: Option<DateTime<Utc>>,
    pub previous_airing: Option<DateTime<Utc>>,
    pub network: Option<S>,
    /// Air time *at location of airing*.
    #[serde(default, deserialize_with = "hm_time")]
    pub air_time: Option<NaiveTime>,
    pub images: Vec<SeriesImage<S>>,
    pub seasons: Vec<Season>,
    pub year: i32,
    pub path: Option<S>, // XXX: Path (?) "/tv/The Big Bang Theory",
    pub profile_id: usize,
    pub season_folder: bool,
    pub monitored: bool,
    pub use_scene_numbering: bool,
    /// Runtime in minutes of one episode
    pub runtime: usize,
    pub tvdb_id: usize,
    pub tv_rage_id: usize,
    pub tv_maze_id: usize,
    pub first_aired: Option<DateTime<Utc>>,
    pub last_info_sync: Option<DateTime<Utc>>,
    pub series_type: SeriesType,
    pub clean_title: S,
    pub imdb_id: Option<S>, // XXX: maybe struct this up "tt0898266",
    pub title_slug: S,
    pub certification: Option<Certification>,
    pub genres: Vec<S>,
    pub tags: Vec<S>,
    pub added: DateTime<Utc>,
    pub ratings: SeriesRating,
    pub quality_profile_id: usize,
    pub id: Option<usize>,
    pub remote_poster: Option<S>,
    #[serde(flatten)]
    pub details: Option<SeriesDetails<S>>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum SeriesStatus {
    Continuing,
    Ended,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "SCREAMING-KEBAB-CASE")]
pub enum Certification {
    #[serde(rename = "TV-14")]
    Tv14,
    TvMa,
    TvPg,
    #[serde(other)]
    Unimplemented,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum SeriesType {
    Standard,
    #[serde(other)]
    Unimplemented, // XXX: Others should be added as items in the enum
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum SeriesImageType {
    Fanart,
    Banner,
    Poster,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SeriesImage<S> {
    pub cover_type: SeriesImageType,
    pub url: S, // XXX: Url ?
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Season {
    pub season_number: usize,
    pub monitored: bool,
    pub statistics: Option<SeasonStatistics>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SeasonStatistics {
    pub next_airing: Option<DateTime<Utc>>,
    pub previous_airing: Option<DateTime<Utc>>,
    pub episode_file_count: usize,
    pub episode_count: usize,
    pub total_episode_count: usize,
    pub size_on_disk: usize,
    pub percent_of_episodes: f32,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SeriesRating {
    pub votes: usize,
    pub value: f32, // XXX: Maybe BigDecimal?
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AlternateTitle<S> {
    pub title: S,
    #[serde(deserialize_with = "minus_one_means_none")]
    pub season_number: Option<usize>,
}

// *Could* be nicer after https://github.com/serde-rs/serde/issues/723
fn hm_time<'de, D>(deserializer: D) -> Result<Option<NaiveTime>, D::Error>
    where D: Deserializer<'de>,
{
    use serde::de::{Error, Unexpected};

    let time: &str = Deserialize::deserialize(deserializer)?;

    Ok(Some(match NaiveTime::parse_from_str(time, "%H:%M") {
        Ok(time) => time,
        Err(_e) => {
            Err(Error::invalid_value(
                    Unexpected::Str(time),
                    &"time of format %H:%M" as &&str))?
        },
    }))
}

fn minus_one_means_none<'de, D>(deserializer: D) -> Result<Option<usize>, D::Error>
    where D: Deserializer<'de>,
{
    Ok(match isize::deserialize(deserializer)? {
        -1 => None,
        i => Some(i as usize),
    })
}

