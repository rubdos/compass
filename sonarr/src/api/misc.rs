use chrono::prelude::*;
use serde::Deserialize;

use crate::api::Series;

pub use echo_utils::api::SystemStatus as CommonSystemStatus;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SystemStatus<S> {
    #[serde(flatten)]
    pub system_status: CommonSystemStatus<S>,

    pub os_name: S,
    pub runtime_name: S,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CalendarItem<S> {
    pub series_id: usize,
    pub episode_file_id: usize,
    pub season_number: usize,
    pub episode_number: usize,
    pub title: S,
    pub air_date: NaiveDate,
    pub air_date_utc: DateTime<Utc>,
    pub overview: Option<String>,
    pub has_file: bool,
    pub monitored: bool,
    pub scene_episode_number: Option<usize>,
    pub scene_season_number: Option<usize>,
    pub tv_db_episode_id: Option<usize>,
    pub series: Series<S>,
    pub downloading: Option<bool>,
    pub id: usize,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Unimplemented<S> {
    pub unimplemented: S,
}
