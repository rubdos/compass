use anyhow::Error;
use thiserror::Error;

use reqwest::StatusCode;

use crate::api::*;

use echo_utils::impl_api;

#[derive(Debug, Error, PartialEq, Eq)]
pub enum ClientError {
    #[error("Transport error")]
    ApiError(echo_utils::ClientError),
}

#[derive(Clone)]
pub struct Client<BaseUrl, Key> {
    inner: echo_utils::Client<BaseUrl, Key>,
}

impl<BaseUrl: AsRef<str>, Key: AsRef<str>> Client<BaseUrl, Key> {
    pub fn new(base_url: BaseUrl, api_key: Key) -> Self {
        Client {
            inner: echo_utils::Client::new(base_url, api_key),
        }
    }

    impl_api! {
        with inner,
        get() "calendar": calendar() -> Vec<CalendarItem<String>>,
        get() "calendar?start={start}&end={end}": calendar_between(start: String, end: String) -> Vec<CalendarItem<String>>,
        get() "calendar?start={start}": calendar_from(start: String) -> Vec<CalendarItem<String>>,
        get() "calendar?end={end}": calendar_until(end: String) -> Vec<CalendarItem<String>>,

        get() "command": commands() -> Vec<Unimplemented<String>>,
        get() "command/{id}": command(id: usize) -> Unimplemented<String>,

        get() "diskspace": diskspace() -> Vec<Unimplemented<String>>,

        get() "episode?seriesId={series_id}": episodes_of(series_id: usize) -> Vec<Unimplemented<String>>,
        get() "episode/{id}": episode(id: usize) -> Unimplemented<String>,
        get() "episodeFile?seriesId={series_id}": episode_file_of(series_id: usize) -> Unimplemented<String>,
        get() "episodeFile/{id}": episode_file(id: usize) -> Unimplemented<String>,

        // XXX: is paginated
        // https://github.com/Sonarr/Sonarr/wiki/History
        // "history?sortKey="
        // "wanted/missing": wanted_missing() -> Unimplemented<String>,

        get() "queue": queue() -> Vec<Unimplemented<String>>,
        get() "parse?title={title}": parse_title(title: String) -> Unimplemented<String>,
        get() "parse?path={path}": parse_path(path: String) -> Unimplemented<String>,
        get() "profile": profiles() -> Unimplemented<String>,
        get() "release?episodeId={episode_id}": releases_of(episode_id: usize) -> Unimplemented<String>,
        get() "rootfolder": root_folder() -> Vec<Unimplemented<String>>,

        get() "series": all_series() -> Vec<Series<String>>,
        get() "series/{id}": series(id: usize) -> Series<String>,
        get() "series/lookup?term={term}": lookup(term: String) -> Vec<Series<String>>,

        get() "system/status": system_status() -> SystemStatus<String>,
        get() "system/backup": backups() -> Vec<Unimplemented<String>>,
        get() "tag": tags() -> Vec<Unimplemented<String>>,
        get() "tag/{id}": tag(id: usize) -> Unimplemented<String>,
    }
}
