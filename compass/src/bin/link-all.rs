use anyhow::Error;

use hts::Client as HtsClient;
use sonarr::Client as SonarrClient;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let sonarr_server = dotenv::var("SONARR_URL").unwrap();
    let api_key = dotenv::var("SONARR_API").unwrap();

    let hts_server = dotenv::var("HTS_URL").unwrap();
    let hts_usr = dotenv::var("HTS_USERNAME").unwrap();
    let hts_pwd = dotenv::var("HTS_PASSWORD").unwrap();

    let sonarr = SonarrClient::new(sonarr_server, api_key);
    let hts = HtsClient::new(hts_server, (hts_usr, hts_pwd));

    let linker = compass::linker::Linker::new(sonarr.clone()).await?;

    for recording in hts.all_recordings().await? {
        let (s, e) = match recording.parse_episode()? {
            None => {
                println!(
                    "Skipping {} -- probably not an episode ({})",
                    recording.disp_title, recording.episode_disp
                );
                continue;
            }
            Some((s, e)) => {
                println!(
                    "Processing recording {} - S{}E{}",
                    recording.disp_title, s, e
                );
                (s, e)
            }
        };

        let series = sonarr.lookup(recording.disp_title.clone()).await?;

        println!("  Found {} matches", series.len());
        println!("  Autorec: {}", recording.autorec);
        for (i, m) in series.iter().enumerate() {
            let (id, score) = linker.link(m, &recording);
            if let Some(id) = id {
                println!(
                    "  - ({}) {} ({:.0}%) on Sonarr {}",
                    i,
                    m.title,
                    score * 100.,
                    id
                );
            } else {
                println!("  -!({}) {} ({:.0}%)", i, m.title, score * 100.);
            }
        }
    }

    Ok(())
}
