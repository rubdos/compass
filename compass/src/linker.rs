use std::hash::Hash;

use anyhow::Error;

use hts::dvr::DvrEntryGrid;
use sonarr::{Client as SonarrClient, Series};

pub struct Linker<C, Li> {
    client: C,

    library: Vec<Li>,
}

impl<S1: AsRef<str>> Linker<SonarrClient<S1, S1>, Series<String>> {
    pub async fn new(
        client: SonarrClient<S1, S1>,
    ) -> Result<Linker<SonarrClient<S1, S1>, Series<String>>, Error> {
        let library = client.all_series().await?;
        Ok(Linker { client, library })
    }

    pub fn link<S2, S3>(
        &self,
        series: &Series<S2>,
        recording: &DvrEntryGrid<S3>,
    ) -> (Option<usize>, f32)
    where
        S2: AsRef<str> + std::fmt::Debug,
        S3: AsRef<str> + Hash + Eq + std::fmt::Debug,
    {
        let id = self
            .library
            .iter()
            .filter(|a| a.tvdb_id == series.tvdb_id)
            .next()
            .and_then(|m| m.id);

        let name_distance = series
            .details
            .iter()
            .map(|details| details.alternate_titles.iter())
            .flatten()
            .map(|title| title.title.as_ref())
            .chain(std::iter::once(series.title.as_ref()))
            .map(|series_title| {
                let rdt = recording.disp_title.as_ref();
                edit_distance::edit_distance(series_title, rdt) as f32
                    / usize::max(series_title.len(), rdt.len()) as f32
            })
            .fold(1., |acc, dist| if dist < acc { dist } else { acc });

        let name_similarity = 1. - name_distance;

        // let channel_similarity = recording.channelname == series.network;

        (id, name_similarity)
    }
}
