use thiserror::Error;

use serde::Serialize;

use reqwest::Client as AsyncClient;
use reqwest::Method;
use reqwest::RequestBuilder;

pub mod api;

macro_rules! impl_methods {
    ($($method:ident -> $verb:ident($($param:ident),*),)*) => {
        $(impl_methods!(IMPL, $method, $verb( $($param),*) ) ;)*
    };
    (IMPL, $method:ident, $verb:ident ()) => {
        pub fn $method<Path: AsRef<str>>(&self, path: Path) -> RequestBuilder {
            self.request_builder(Method::$verb, path)
        }
    };
    (IMPL, $method:ident, $verb:ident (data)) => {
        pub fn $method<Path: AsRef<str>, D, S>(&self, path: Path, data: D) -> RequestBuilder
            where D: AsRef<S>,
                  S: Serialize,
        {
            self.request_builder(Method::$verb, path)
                .json(data.as_ref())
        }
    };
}

#[macro_export]
macro_rules! impl_api {
    (with $client_field:ident, $($verb:ident ($($post_param:ident: $ser:ty),*) $path:tt : $method:ident ( $($param:ident: $get_param_type:ty),* ) -> $deser:ty,)*) => {
        $(impl_api!(with $client_field, $verb ($($post_param : $ser),*) $path : $method ($( $param : $get_param_type ),*) -> $deser);)*
    };
    (with $client_field:ident, $verb:ident ($($post_param:ident: $ser:ty),*) $path:tt : $method:ident ( $($param:ident: $get_param_type:ty),* ) -> $deser:ty) => {
        pub async fn $method(&self $(, $param : $get_param_type)*) -> Result<$deser, Error> {
            let url = format!($path, $($param = $param),*);
            let request = self.$client_field.$verb(url $(, $post_param)*);
            let mut reply = request.send().await?;

            if ! reply.status().is_success() {
                match reply.status() {
                    StatusCode::OK => (),
                    StatusCode::FORBIDDEN => Err($crate::ClientError::Unauthorized)?,
                    _ => unimplemented!("StatusCode {:?}", reply.status()),
                }
            }

            Ok(reply.json().await?)
        }
    };
}

#[derive(Clone, Debug)]
pub struct Client<BaseUrl, Key> {
    inner: AsyncClient,
    base_url: BaseUrl,
    api_key: Key,
}

impl<BaseUrl: AsRef<str>, Key: AsRef<str>> Client<BaseUrl, Key> {
    pub fn new(base_url: BaseUrl, api_key: Key) -> Self {
        Client {
            inner: AsyncClient::new(),
            base_url,
            api_key,
        }
    }

    fn request_builder<Path>(&self, method: Method, path: Path) -> RequestBuilder
    where
        Path: AsRef<str>,
    {
        let base_url = self.base_url.as_ref();
        let url = if base_url.ends_with('/') {
            format!("{}api/{}", base_url, path.as_ref())
        } else {
            format!("{}/api/{}", base_url, path.as_ref())
        };

        self.inner
            .request(method, &url)
            .header("X-Api-Key", self.api_key.as_ref())
    }

    impl_methods! {
        get -> GET(),
        post -> POST(data),
        put -> PUT(data),
        delete -> DELETE(),
    }
}

#[derive(Debug, Error, PartialEq, Eq)]
pub enum ClientError {
    #[error("Server returned invalid data.")]
    InvalidData,
    #[error("Invalid credentials.")]
    Unauthorized,
}
