//! Common API structs

use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SystemStatus<S> {
    pub version: S,
    pub build_time: S,
    pub is_debug: bool,
    pub is_production: bool,
    pub is_admin: bool,
    pub is_user_interactive: bool,
    pub startup_path: S,
    pub app_data: S,
    pub os_version: S,
    pub is_mono_runtime: bool,
    pub is_mono: bool,
    pub is_linux: bool,
    pub is_osx: bool,
    pub is_windows: bool,
    pub branch: S,
    pub authentication: S,
    pub sqlite_version: S,
    pub url_base: S,
    pub runtime_version: S,
}

