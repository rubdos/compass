# echo-utils

This Rust crate provides the common bindings for Radarr, Sonarr and probably Lidarr web APIs.
If this crate has a name collision with something you think is a more appropriate crate for this name,
please [file an issue](https://gitlab.com/rubdos/compass/issues/).
