use anyhow::Error;
use thiserror::Error;

use reqwest::StatusCode;

use crate::api::*;

use echo_utils::impl_api;

#[derive(Debug, Error, PartialEq, Eq)]
pub enum ClientError {
    #[error("Transport error")]
    ApiError(echo_utils::ClientError),
}

#[derive(Clone)]
pub struct Client<BaseUrl, Key> {
    inner: echo_utils::Client<BaseUrl, Key>,
}

impl<BaseUrl: AsRef<str>, Key: AsRef<str>> Client<BaseUrl, Key> {
    pub fn new(base_url: BaseUrl, api_key: Key) -> Self {
        Client {
            inner: echo_utils::Client::new(base_url, api_key),
        }
    }

    impl_api! {
        with inner,
        get() "calendar": calendar() -> Vec<Movie<String>>,
        get() "calendar?start={start}&end={end}": calendar_between(start: String, end: String) -> Vec<Movie<String>>,
        get() "calendar?start={start}": calendar_from(start: String) -> Vec<Movie<String>>,
        get() "calendar?end={end}": calendar_until(end: String) -> Vec<Movie<String>>,

        get() "movie": movies() -> Vec<Movie<String>>,
        get() "movie?id={}": movie(id: usize) -> Vec<Movie<String>>,

        get() "movie/lookup?term={term}": lookup(term: String) -> Vec<MovieLookup<String>>,

        get() "system/status": system_status() -> SystemStatus<String>,
    }
}
