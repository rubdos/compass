use chrono::prelude::*;
use serde::Deserialize;

pub use echo_utils::api::SystemStatus;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Movie<S> {
    #[serde(flatten)]
    pub common: MovieDetails<S>,

    pub id: usize,
    pub last_info_sync: DateTime<Utc>,
    pub clean_title: S,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MovieLookup<S> {
    #[serde(flatten)]
    pub common: MovieDetails<S>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MovieDetails<S> {
    pub title: S,
    pub sort_title: S,
    pub size_on_disk: usize,
    pub status: S, // XXX: enum
    pub overview: Option<String>,
    pub in_cinemas: Option<DateTime<Utc>>,
    pub images: Vec<MovieImage<S>>,
    pub website: Option<S>,
    pub downloaded: bool,
    pub year: i32,
    pub has_file: bool,
    #[serde(rename = "youTubeTrailerId")]
    pub youtube_trailer_id: Option<S>,
    pub studio: Option<S>,
    pub path: Option<S>,
    pub profile_id: usize,
    pub monitored: bool,
    pub minimum_availability: S, // XXX: enum?
    pub runtime: usize,
    pub imdb_id: Option<S>,
    pub tmdb_id: Option<usize>,
    pub title_slug: S,
    pub genres: Vec<S>,
    pub tags: Vec<S>,
    pub added: DateTime<Utc>,
    pub ratings: Ratings,
    pub alternative_titles: Vec<AlternativeTitle<S>>,
    pub quality_profile_id: usize,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MovieImage<S> {
    cover_type: S,
    url: S,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Ratings {
    votes: usize,
    value: f32,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AlternativeTitle<S> {
    source_type: S, // enum tmdb/imdb
    movie_id: usize,
    title: S,
    source_id: usize,
    votes: usize,
    vote_count: usize,
    language: S,
    id: usize,
}
