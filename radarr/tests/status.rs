use anyhow::*;
use radarr::*;

fn default_client() -> Client<String, String> {
    let server = dotenv::var("RADARR_URL").unwrap();
    let api_key = dotenv::var("RADARR_API").unwrap();

    Client::new(server, api_key)
}

#[tokio::test]
async fn movies() -> Result<(), Error> {
    let client = default_client();

    let movies = client.movies().await?;

    for movie in movies {
        let _movie = client.movie(movie.id).await?;
    }
    Ok(())
}

#[tokio::test]
async fn movie_lookup() -> Result<(), Error> {
    let client = default_client();

    let movie = "Big Buck Bunny".into();

    let candidates = client.lookup(movie).await?;
    assert!(candidates.len() > 0);

    Ok(())
}

#[tokio::test]
async fn simple_get() -> Result<(), Error> {
    let client = default_client();

    macro_rules! do_test {
        ($($method:ident,)*) => {
            $(do_test!(IMPL $method);)*
        };
        (IMPL $method:ident) => {
            println!("Testing {}", stringify!($method));
            let _result = client.$method().await?;
        };
    }

    do_test! {
        system_status,
        calendar,
        movies,
    }
    Ok(())
}

#[tokio::test]
async fn calendar_range() -> Result<(), Error> {
    // use chrono::prelude::*;

    let client = default_client();

    // XXX: hard coded days
    // TODO: range today to today + 1 month
    let _calendar = client
        .calendar_between("2019-03-30".into(), "2019-04-30".into())
        .await?;
    Ok(())
}
