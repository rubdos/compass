[![pipeline status](https://gitlab.com/rubdos/compass/badges/master/pipeline.svg)](https://gitlab.com/rubdos/compass/commits/master)
[![coverage report](https://gitlab.com/rubdos/compass/badges/master/coverage.svg)](https://gitlab.com/rubdos/compass/commits/master)

# *Compass* directs your recordings

(Will be) all you need to postprocess your TVHeadend recordings.

## Project goals:

- Transcoding
- Moving files
- Calling compskip
- Registration in Sonarr/Radarr

## Miscellaneous

This repository is the home of a few Rust crates that wrap API's:

- `hts-rs` wraps the TVHeadend (web) API.
  I would be interested in having HTSP support for other projects.
- `sonarr` are bindings to the Sonarr web API.
- `radarr` are bindings to the Radarr web API.
- `echo-utils` wraps what's common to the `sonarr` and `radarr` crates.

I would be okay with having a Lidarr wrapper in this repository.

If anyone ever feels offended that these libraries are in a single repository,
it is probably time to split them up a bit.
Feel free to make an issue.
