use anyhow::Error;
use hts::Client;

#[tokio::test]
async fn list_recordings() -> Result<(), Error> {
    let server = dotenv::var("HTS_URL").unwrap();
    let username = dotenv::var("HTS_USERNAME").unwrap();
    let password = dotenv::var("HTS_PASSWORD").unwrap();

    let client = Client::new(server, (username, password));

    let recording_queries = vec![
        client.finished_recordings().await?,
        client.upcoming_recordings().await?,
        client.failed_recordings().await?,
        client.removed_recordings().await?,
    ];

    let all_recordings = client.all_recordings().await?;

    let count: usize = recording_queries.iter().map(Vec::len).sum();
    assert_eq!(count, all_recordings.len());
    Ok(())
}

#[tokio::test]
async fn unauthorized() -> Result<(), Error> {
    let server = dotenv::var("HTS_URL").unwrap();
    let username = "nonexistent-user";
    let password = "garbage password";

    let client = Client::new(server, (username, password));

    let recordings = client.finished_recordings().await;
    let error = recordings.expect_err("Unauthorized");
    let error: hts::HtsClientError = error.downcast().expect("HtsClientError");

    assert_eq!(error, hts::HtsClientError::Unauthorized);
    Ok(())
}
