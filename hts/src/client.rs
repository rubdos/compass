use core::ops::AddAssign;

use anyhow::Error;
use thiserror::Error;

use serde::{Deserialize, Serialize};

use reqwest::Client as AsyncClient;
use reqwest::RequestBuilder;
use reqwest::{Method, StatusCode};

#[derive(Debug, Deserialize, Serialize)]
struct VectorResponse<S> {
    total: usize,
    entries: Vec<S>,
}

impl<S> AddAssign for VectorResponse<S> {
    fn add_assign(&mut self, other: Self) {
        self.total = other.total;
        self.entries.extend(other.entries);
    }
}

#[derive(Debug, Error, PartialEq, Eq)]
pub enum HtsClientError {
    #[error("Server returned invalid data.")]
    InvalidData,
    #[error("Invalid credentials.")]
    Unauthorized,
}

#[derive(Clone)]
pub struct Client<BaseUrl, U, P> {
    inner: AsyncClient,
    base_url: BaseUrl,
    credentials: Option<(U, P)>,
}

macro_rules! impl_methods {
    ($($method:ident -> $verb:ident,)*) => {
        $(impl_methods!(IMPL, $method, $verb);)*
    };
    (IMPL, $method:ident, $verb:ident) => {
        fn $method<Path: AsRef<str>>(&self, path: Path) -> RequestBuilder {
            self.request_builder(Method::$verb, path)
        }
    }
}

macro_rules! impl_api_get {
    ($($path:tt : $method:ident -> $collection:ident $deser:ty,)*) => {
        // "dvr/entry/grid_finished": finished_recordings -> VEC DvrEntryGrid,
        $(impl_api_get!($collection $path : $method -> $deser);)*
    };
    (VEC $path:tt : $method:ident -> $deser:ty) => {
        pub async fn $method(&self) -> Result<Vec<$deser>, Error> {
            // Parse status code when it's somehow possible to grab it and return an error.
            let mut response = VectorResponse {
                total: 0,
                entries: vec![],
            };

            loop {
                let url = format!("{}?start={}", $path, response.entries.len());
                let request = self.get(url);
                let reply = request.send().await?;
                if ! reply.status().is_success() {
                    match reply.status() {
                        StatusCode::OK => (),
                        StatusCode::FORBIDDEN => Err(HtsClientError::Unauthorized)?,
                        _ => unimplemented!("StatusCode {:?}", reply.status()),
                    }
                }

                // https://github.com/rust-lang/rust/issues/48048
                let partial = reply.json().await?;
                response += partial;

                if response.entries.len() >= response.total {
                    break;
                }
            }
            let entries = response.entries;
            Ok(entries)
        }
    }
}

impl<BaseUrl: AsRef<str>, U: AsRef<str>, P: AsRef<str>> Client<BaseUrl, U, P> {
    pub fn new<Credentials>(base_url: BaseUrl, credentials: Credentials) -> Client<BaseUrl, U, P>
    where
        Credentials: Into<Option<(U, P)>>,
    {
        Client {
            inner: AsyncClient::new(),
            base_url,
            credentials: credentials.into(),
        }
    }

    fn request_builder<Path>(&self, method: Method, path: Path) -> RequestBuilder
    where
        Path: AsRef<str>,
    {
        let url = format!("{}/api/{}", self.base_url.as_ref(), path.as_ref());

        let request = self.inner.request(method, &url);

        match self.credentials.as_ref() {
            Some(&(ref username, ref password)) => {
                request.basic_auth(username.as_ref(), Some(password.as_ref()))
            }
            None => request,
        }
    }

    impl_methods! {
        get -> GET,
        post -> POST,
    }

    // DVR
    impl_api_get! {
        // https://github.com/dave-p/TVH-API-docs/wiki/Common-Parameters#grid-parameters
        "dvr/entry/grid": all_recordings -> VEC crate::dvr::DvrEntryGrid<String>,
        "dvr/entry/grid_finished": finished_recordings -> VEC crate::dvr::DvrEntryGrid<String>,
        "dvr/entry/grid_upcoming": upcoming_recordings -> VEC crate::dvr::DvrEntryGrid<String>,
        "dvr/entry/grid_failed": failed_recordings -> VEC crate::dvr::DvrEntryGrid<String>,
        "dvr/entry/grid_removed": removed_recordings -> VEC crate::dvr::DvrEntryGrid<String>,
    }
}
