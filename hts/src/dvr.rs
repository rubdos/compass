use std::collections::HashMap;
use std::hash::Hash;

use anyhow::{ensure, Error};

use chrono::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct DvrEntryGrid<S: Hash + Eq> {
    pub comment: Option<S>,
    pub creator: S,
    pub disp_description: S,
    pub start_extra: u32,
    pub stop_extra: u32,
    pub noresched: bool,
    pub enabled: bool,
    pub description: Option<HashMap<S, S>>,
    pub title: HashMap<S, S>,
    #[serde(default)]
    pub subtitle: HashMap<S, S>,

    #[serde(with = "chrono::serde::ts_seconds")]
    pub create: DateTime<Utc>,
    #[serde(with = "chrono::serde::ts_seconds")]
    pub start: DateTime<Utc>,
    #[serde(with = "chrono::serde::ts_seconds")]
    pub start_real: DateTime<Utc>,
    #[serde(with = "chrono::serde::ts_seconds")]
    pub stop: DateTime<Utc>,
    #[serde(with = "chrono::serde::ts_seconds")]
    pub stop_real: DateTime<Utc>,

    pub duplicate: u32,
    #[serde(with = "chrono::serde::ts_seconds")]
    pub removal: DateTime<Utc>,
    pub dvb_eid: u64,
    #[serde(default)]
    pub channel_icon: Option<S>,
    pub channelname: S,
    pub autorec: S,
    pub config_name: S,
    pub timerec: S,
    pub content_type: u32,
    pub parent: S,
    pub disp_subtitle: S,
    pub filename: S,
    pub errorcode: u32, // XXX
    pub duration: u32,
    pub timerec_caption: S,
    pub child: S,
    pub errors: u64,
    pub playcount: u64,
    pub norerecord: bool,
    pub url: S,
    pub playposition: u32,
    pub data_errors: u64,
    pub sched_status: S, // XXX
    pub broadcast: u64,
    pub status: S, // XXX ?
    pub disp_title: S,
    pub filesize: u64,
    pub pri: u32,
    pub channel: S,
    pub uuid: S,
    pub fileremoved: u32,
    pub autorec_caption: S,
    pub owner: S,
    pub retention: u32,
    pub episode_disp: S,
}

impl<S: AsRef<str> + Hash + Eq> DvrEntryGrid<S> {
    pub fn parse_episode(&self) -> Result<Option<(usize, usize)>, Error> {
        let episode = self.episode_disp.as_ref();
        if episode.trim().is_empty() {
            return Ok(None);
        }

        let parts = episode.split('.').collect::<Vec<_>>();
        if parts.len() == 2 {
            ensure!(
                parts[0].starts_with("Season") && parts[1].starts_with("Episode"),
                "Unrecognised Season.Episode format: `{}'",
                episode
            );

            let season = &parts[0][("Season ".len())..];
            let episode = &parts[1][("Episode ".len())..];

            return Ok(Some((season.parse()?, episode.parse()?)));
        } else {
            ensure!(parts.len() == 1, "Unknown episode format: `{}'", episode);
            ensure!(
                parts[0].starts_with("Episode"),
                "Expected EpisodeXX, got `{}'",
                episode
            );

            let episode = &parts[0][("Episode ".len())..];
            return Ok(Some((0, episode.parse()?)));
        }

        Ok(None)
    }

    pub fn season(&self) -> Result<Option<usize>, Error> {
        Ok(self.parse_episode()?.map(|a| a.0))
    }

    pub fn episode(&self) -> Result<Option<usize>, Error> {
        Ok(self.parse_episode()?.map(|a| a.1))
    }
}
