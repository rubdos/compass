# HTTP API bindings for TVHeadend

Current features:

- list recordings

For my personal requirements, I will not need much more than simple recordings management, but I'm open for any other contributions (including HTSP support).
